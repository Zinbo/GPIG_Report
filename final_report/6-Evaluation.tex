%!TEX root = main.tex
\section{Evaluation}
\label{sec:eval}
During the first prototype phase the Customer provided us with an application that they wish to monitor using our HUMS. In order to test the flexibility of the first prototype, we also implemented Plugins that monitored the availability of a web server by pinging it periodically.

During the development of the second prototype, the Customer provided us with another application that they wish to monitor with our HUMS. Unlike the first application, it was not written in Java, therefore we could prove that our architecture was tailorable enough to support another environment. Additionally the team came up with two additional scenarios in order to evaluate the second prototype in various circumstances.

This section will evaluate the system working in each of the domains that we have implemented Plugins for, detailing the input to the system, the analysis that is performed, and the format that the results are presented in. The subsection will conclude with a discussion of the extent to which our system has met the requirements, and an account of future work intended for a subsequent prototype.

\subsection{Test Application 1}
During the first prototype we implemented a JVM monitoring solution that made use of JMX. The customer has previously expressed an interest in the HUMS being able to utilise common communication protocols, such as SNMP. As well as monitoring using JMX, the JVM can also be monitored using SNMP \cite{snmpvsjmx}

For this prototype, we have migrated the JMX monitoring Plugins to the new architecture, and also implemented SNMP monitoring of the JVM.

To recap, the application provided by the customer takes no input, nor does it produce any output. During the previous phase of development we observed that the test application increases its memory consumption over time, and eventually throws an exception when the maximum heap size is exceeded. We decided to monitor the amount of heap memory that was being used by the application, and report when the application was using more than 75\% of the maximum heap memory size. We also checked that the application was still running, and reported to the user if this was no longer the case.
\subsubsection{JMX}
The code for the JMX monitoring plugins remains largely unchanged. The 3 Plugins (sense, analyse and report) have been adapted to the modified architecture, and where appropriate the configuration system is now utilised, rather than the hard coding of variables that was done in the first prototype. In particular, the host and port that are used to connect to the JVM using JMX are retrieved from the configuration system, making the JVM JMX monitoring Plugins a lot more flexible.
The output of the JMX monitoring remains the same - a simple logging system that writes to the standard output (typically the console in our system) when there is anything to report.

To run the JMX monitoring system, a scheduled job must be created that includes the following tasks (in this order), and the associated parameters:

\begin{samepage}
\begin{description}
	\item[Sense] JvmMonitor
	\begin{description}
	\item[host]	127.0.0.1 (typically)
	\item[port]	8181 (typically)
	\end{description}
	\item[Analysis]	JvmAnalysis
\item[Report] Logger
\end{description}
\end{samepage}

The sense Plugin in this case has two parameters that must be set, host and port. In our setup the standard localhost IP was used, although this can be changed if the application is to be monitored remotely. In addition to this, the Java application that is to be monitored must be started with some additional parameters. Our test application was called \verb|b.jar|, which is why the following ends with b.jar:

\begin{lstlisting}[numbers=none]
java -Xmx40m -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=8181 -Dcom.sun.management.jmxremote.local.only=false -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -jar b.jar
\end{lstlisting}

Output obtained at three different stages of monitoring the test application is included in Figures \ref{fig:jvmjmx1} - \ref{fig:jvmjmx3}.

\begin{figure*}
\centering
\includegraphics[width=.75\textwidth,height=\textheight,keepaspectratio]{./figures/screenshots/jvm-jmx-1}
\caption{The output from the system when the JVM JMX monitor is running. At the bottom of the screenshot you see that the application is running, and using about 18\% of its allocated heap memory}
\label{fig:jvmjmx1}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=.75\textwidth,height=\textheight,keepaspectratio]{./figures/screenshots/jvm_jmx_2}
\caption{The output from the system when the JVM JMX monitor is running and the memory usage is approach critical levels.}
\label{fig:jvmjmx2}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=.75\textwidth,height=\textheight,keepaspectratio]{./figures/screenshots/jvm_jmx_3}
\caption{The output from the system when the JVM JMX monitor is running but the application being monitored has exited because of an error.}
\label{fig:jvmjmx3}
\end{figure*}

\subsection{SNMP}
SNMP monitoring implements the same monitoring functionality as the JMX monitoring plugin - memory usage monitoring, and the running status of the application being monitored. A new reporting Plugin has been implemented that sends an email to an address specified in the configuration when the application exits. 

To run the SNMP monitoring, a job must be created with the following tasks (in this order) and parameters:

\begin{description}
\item[Sense] JvmMonitorSNMP
\item[Analysis] JvmAnalysis
\item[Report] JvmEmailer
\end{description}

Because of the loose coupling that exists between the sense and analysis Plugins, the same analysis Plugin is used here as is used for JMX monitoring.

The screenshot in Figure \ref{fig:email} shows the email that is sent when the program being monitored ends.

\begin{figure*}
\centering
\includegraphics[width=.95\textwidth,height=\textheight,keepaspectratio]{./figures/screenshots/email}
\caption{The email that is sent by the HUMS}
\label{fig:email}
\end{figure*}

\subsection{Test Application 2}
The second application to be provided from the customer does not make use of the JVM, and therefore the monitoring tools that were previously described cannot be reused. No information on the languages or technologies used to implement the application were provided, and so a more generic approach to monitoring applications was taken. 

Java is a very flexible language that can run on many operating systems, which is one of the reasons that we chose to use it for creating the HUMS. However, it is easy to imagine a situation where it is not possible to use Java to monitor a system, and so another language must be used. To demonstrate that our HUMS can deal with this situation, we chose to write the monitoring code for Test Application 2 in C\#. 

Like the second test application, the C\# monitoring code runs on Linux only (although it would be trivial to port it to Windows), and makes use of Linux's process monitoring tools to monitor the memory usage and running status, like the first test application.

This example was used to present another feature of the second prototype - support for pushed messages. The C\# monitoring application is considered to be a sensor in this case and it sends us data when it wants to. Normally it would be implemented by a Third Party developer and he would specify the conditions under which a message with relevant data should be pushed to the HUMS.

The IronMQ message queue service was used to transfer information from the C\# monitoring application to the HUMS. This adds another abstraction layer to our system and allows for greater flexibility -- a change in HUMS' implementation would have no effect on the sensor application. IronMQ delivers all messages to the IronMQ Handler (as shown in \autoref{fig:ironMqSeq}) that creates a Job consisting of Tasks according to the configuration specified for the given sensor ID. Report Plugin is used to plot graphs of the test application's statistics and save them as pdf files.

The graphs showed that the application demonstrated very few significant characteristics. The memory and IO data showed that the application simply loaded initial values at the beginning of the execution. No further memory allocation or disk IO was performed. The processor user time did however increase at a constant rate. Further analysis of the data collected showed that the application used as much computation resource on a single thread as was allocated to it by the operating system. A sample of each graph created by the system is provided in Figures \ref{test2output} and \ref{test2output2}.

\begin{figure*}
\begin{minipage}{0.49\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./figures/graphs/CharactersRead}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./figures/graphs/CharactersWritten}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./figures/graphs/ReadSyscalls}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./figures/graphs/WriteSyscalls}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./figures/graphs/UserTime}
\end{minipage}
\begin{minipage}{0.49\textwidth}
	\centering
	\includegraphics[width=\linewidth]{./figures/graphs/VirtualMemSize}
\end{minipage}
\caption{Some of the output from Test Application 2 monitoring\label{test2output}}
\end{figure*}

\begin{figure*}
	\centering
	\includegraphics[width=0.49\linewidth]{./figures/graphs/VmSize}
	\caption{Further output from the Test Application 2 monitoring\label{test2output2}}
\end{figure*}

To run the C\# monitoring application, on a Linux machine navigate to the TestApplication2 folder and execute:

\verb|./run.sh|

The HUMS must then be configured with the following Plugins (in this order) and parameters:

\begin{description}
\item[Sense] ProcReader
\item[Analyse] ProcAnalysis
\item[Report] KeyValueGrapher
	\begin{description}
	\item[Y]	stats.VirtualMemSize
	\end{description}
\item[Report] KeyValueGrapher
	\begin{description}
	\item[Y] memoryStats.VmSize
	\end{description}
\item[Report] KeyValueGrapher
	\begin{description}
	\item[Y] ioStats.CharactersRead
	\end{description}
\item[Report] KeyValueGrapher
	\begin{description}
		\item[Y] ioStats.CharactersWritten
	\end{description}
\item[Report] KeyValueGrapher
	\begin{description}
	\item[Y] ioStats.Readsyscalls
	\end{description}
\item[Report] KeyValueGrapher
	\begin{description}
		\item[Y] ioStats.Writesyscalls
	\end{description}	
\end{description}

The screenshot in Figure \ref{fig:testapp2csharp} shows the output from the C\# application that is monitoring the test application and pushing the data to the HUMS.

\begin{figure*}
\centering
\includegraphics[width=.9\textwidth,height=\textheight,keepaspectratio]{./figures/screenshots/testapp2sensor}
\caption{The output from the C\# application that monitors the second test application}
\label{fig:testapp2csharp}
\end{figure*}

\subsection{Ping}
For the first prototype we developed a set of Plugins that `pinged' a web server and checked that its response time was acceptable. The definition of acceptable was hard-coded as \textless{} 1000ms. This is a relatively simple demonstration of the HUMS, but could still prove useful to a systems administrator.

For the second prototype, we ported the sense, analysis and report Plugins to the new architecture. The Plugins have been altered to include a feedback loop for use with our own system. The HUMS is pinged by itself at regular intervals, and if it finds that the response time is unacceptable (i.e. the system is overloaded) then the PingReport Plugin forces the Scheduler to cancel some jobs so that it can `catch up' with any outstanding jobs. This functionality is not perfect because some critical jobs could be cancelled, but for the purposes of a prototype, it is sufficient to show that the system can implement a feedback loop. The next prototype could possibly decide which jobs should be cancelled or delayed, and make jobs of higher importance execute first.

The HUMS must be configured with the following Plugins (in this order) and parameters:
\begin{description}
\item[Sense] Ping
\item[Analysis] PingAnalysis
\item[Report] PingReport
\end{description}

\subsection{Weather Monitoring}

The University of York's Electronics department has a weather sensor that posts its readings to a web page \cite{weather}. To further demonstrate the flexibility of the domains that our HUMS can work in, the weather plugin scrapes the data from this page every 60 seconds (matching the frequency that the page is updated) and stores it in our database. There is a lot of information that is fetched from this page, including the amount of rainfall that day, wind speed, wind direction, temperature etc. 

An analysis Plugin has been implemented that checks the daily rainfall amount to determine if it has started raining or not. To do this, it has to use historical data from the database to see if the amount of rainfall was changing before, and whether or not it is still changing or not changing (showing that analysis can make use of historical data, as well as current data from the sense Plugin). 

A Plugin that can post to a Twitter account through the Twitter API has been implemented. The Plugin has been extended to post tweets when it starts or stops raining (as determined by the analysis Plugin). 

To configure the HUMS to work as a weather monitoring system, a job with the following tasks and settings (in this order) should be setup:

\begin{description}
\item[Sense] WeatherMonitor
	\begin{description}
		\item[uri] http://weather.elec.york.ac.uk/
		\item[selector]	table[summary\$=Data]
	\end{description}
\end{description}
	
A second job should also be setup:

\begin{description}
\item[Analysis]	WeatherRainStatusAnalysis
	\begin{description}
	\item[sensorID]	(the ID of the WeatherMonitor sensor)
	\end{description}	
\item[Report] WeatherRainTweeter
\end{description}

Having two jobs that run independently shows that data from one job can be accessed by another job, just by knowing the ID of the first job.

A screenshot of the weather monitor output can be seen in Figure \ref{fig:twitteroutput}.

\begin{figure*}
\centering
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{figures/screenshots/twitterOutput}
\caption{The test Twitter account that we used to test the weather reporting Plugin}
\label{fig:twitteroutput}
\end{figure*}

\subsection{Stock Monitoring}
The final set of Plugins that we implemented in order to prove the flexibility of our HUMS was a stock value monitor. Configured with a certain publicly listed company's stock name (such as AAPL), the sense Plugin will periodically fetch the value of the stock and store it. The fetching is done by a third-party library called java-stocks which was modified slightly for use in our system. The analysis Plugin checks the stock value of the company against a certain value, and if it is lower than a certain price then outputs a message to the console informing the user that they should buy the stock being monitored.

A second, more interesting analysis was to try and incorporate some form of prediction, such as machine learning, to the stock value monitor. Unfortunately, finding a source of real time stock data for free is difficult. Such real-time sources of stock data are highly valuable and you must pay for them. Instead, historical stock data were acquired from Yahoo! finance to demonstrate the flexibility of our HUMS. These data were downloaded and then analysed off-line. The results of the analysis were then shown through our flexible reporting interface.

A first analysis attempt was to train a neural network over the historical stock data. The time taken for the neural network to converge on the data we were able to collect was prohibitive. This was a combination of poor feature engineering and a poor library choice.

Instead, linear regression was employed. This is a much simpler form of machine learning. The method was trained using historical data about the NASDAQ exchange and the Apple company. This analysis was implemented so that it could easily be adapted to train over real-time stock data instead of historical data. The result of the analysis is a regression model which given the difference in closing prices on the NASDAQ for a given set of consecutive days will predict the same difference in the Apple stock (APPL) -- the corresponding graph is shown in \autoref{fig:stockMon}. The company to predict from the NASDAQ may be configured.

The architecture of our system means that it could potentially become the platform for a high frequency trading system. A set of sense Plugins would gather various bits of data that affect the price of stocks, and then a clever analysis Plugin would determine whether or not stock should be bought or sold, based on the factors collected by the sense Plugins. The reporting Plugins would either make the trades automatically, or inform the trader of its conclusions. The system would need a lot of optimisation if it were to be used in the real world as a high frequency trading platform, but the overall architecture of the system means that it is feasible.

To configure the HUMS to work as a stock monitoring system, a job with the following tasks (in this order) should be setup:

\begin{description}
\item[Sense] StockMonitor
\item[Analysis]	StockAnalysis
\item[Report] StockReport
\end{description}

\begin{figure*}
\centering
\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{./figures/screenshots/stockOutput}
\caption{The output of the stock monitor}
\label{fig:stockMon}
\end{figure*}

\subsection{Summary of Test Application} % (fold)
\label{sub:summary_of_test_application}
The implemented test applications show how powerful and tailorable our HUMS is. It is possible to develop all the functionality within the system boundaries in custom Plugins and poll for data or create thin Plugins to which data is pushed by external devices or applications. It is also possible for Jobs to share data which means that one sensor can be used for several purposes. Furthermore, reporting capabilities are only restricted by the Third Party developer's skill set. As shown in the test applications our HUMS can store pdf files, publish messages on Twitter, send emails and return encoded data to the frontend. In conclusion the Customer's main requirement -- system's tailorability -- has been met.
% subsection summary_of_test_application (end)

\subsection{Review of Requirements}
Each of the subsections from the requirements table will be evaluated, with a discussion on what was implemented from each, and what was not.

\subsubsection{Main} % (fold)
\label{ssub:main}

% subsubsection main (end)Main
The main subsection of requirements was used for all requirements that related to the general functionality of the HUMS. Specifically these were requirements that related to the scheduler, user interface and configuration system.

Requirement MAIN-1 was that there is an API that reporting Plugins can use to send commands to sensors. When we thought about this requirement in more depth we concluded that it was in fact too high level, and would be very difficult to implement an API that accepted commands for all types of sensors. Therefore we scrapped this requirement, and any reporting Plugin that wishes to communicate with sensors will have to do so by itself. One workaround could be that the sense Plugin for the sensor in question could implement static methods that send commands. Alternatively the job could be setup so that the sense Plugin task is executed again after the report Plugin task, and the report Plugin task places the commands to send to the sensor in the JobData object, which can be read by the sense Plugin task.

MAIN-13 and MAIN-14, to order scheduled jobs by priority, was another scheduler related task that was never implemented, but the reasoning behind this was discussed in the previous subsection. MAIN-15 was that the scheduler shall have the ability to adjust the start times of tasks so as to better distribute the load of the system and prevent missed deadlines. This was not implemented as a part of the core architecture, but the PingAdjust Plugin showed that it is possible to do with our system.

MAIN-2 relates to the logging system, in that whenever a Plugin `times-out' it should be logged so that the user can see. When this requirement was added, we expected that each Plugin would have a strict limit on how long it had to execute, and that if this limit is overrun then an error would be logged. This approach would be suitable if our HUMS was a critical real-time system, but that is not the case, and so this requirement was not fulfilled.

MAIN-19 states that the system itself shall be monitorable. This is unfortunately another vague requirement that did not specify exactly what aspects of the system should be monitorable. One of the test applications was a JVM monitor, and as our system itself executes in a JVM instance, we could configure the JVM monitor to monitor our system. Because of time limitations this was not tested out, but there is no reason in theory why this would not work.

\subsubsection{Sense/Store/Report} % (fold)
\label{ssub:sense}

% subsubsection sense (end)Sense
The remaining requirements were split into one of four subsections: sense, analysis, report and store. Again each requirement that has not been satisfied will be explored, and the reason it has not been satisfied will be explained.

SENSE-1 requires that sense Plugins should be added at runtime. Each of Sense, Analysis and Report have a similar requirement, but unfortunately neither of them have been satisfied. A more detailed description of this is given in the next subsection that details problems that were encountered during development. The same applies for SENSE-2, SENSE-3, and the same requirements but for analysis and reporting.

SENSE-4 is that we should be able to define how long data are stored for. This is probably one of the more trivial requirements, but we have unfortunately not had chance to implement it. It was decided that the bigger requirements (such as implementing a scheduler) were of more importance, so these got more attention. To implement this requirement, a cleanup operation could be scheduled to run once a day that goes through each sense Plugin and deletes any data from the database that is older than the sense Plugin states should be stored.

SENSE-6 states that a timeout period for Plugin execution should be added, and any Plugins that exceed the timeout limit should be terminated. (Again, this requirement is duplicated in the report and analysis subsections.) Currently this has not been implemented, partly because of time constraints, but partly because of the way our scheduler works. The jobs that are executed are re-used each time a scheduled job is executed, and so when the scheduler fires a job, the job resets itself before executing each task. This means that a job will automatically time-out when its next scheduled execution happens (i.e. the timeout value = period of the job).  To implement this requirement properly, the scheduler would have to start the RecurringJob on a new thread (which it already does), but then the RecurringJob's execute method would need to start the Job on another thread, and the RecurringJob thread monitor that a timeout doesn't occur. As Job already implements Runnable, this would not be a big task.

REPORT-4 states that `the reporting subsystem shall allow reporting Plugins to generate reports after a configurable delay'. While designing our system we realised that this adds un-necessary complexity to our program. Instead of configuring a delay before report generation, the user can simply setup one job to perform sense and analysis tasks with a certain frequency, and then the reporting task can be put in another job with a different frequency. This is demonstrated by the weather monitoring Plugins, where the sensing has one period, but the analysis and reporting have a separate reporting period.

REPORT-11 is a vague requirement that states that the user should be notified when a report is generated. After further consideration we decided that notifying the user is a job for each report Plugin to implement. The logging system makes a log when a task is executed, and so technically this requirement has been fulfilled.

STORE-3 requires that our system accepts manual uploading of data. This was deemed to be low priority, and again due to time constraints was not implemented. There is no reason why a sense Plugin couldn't be implemented that imports data to the database from some file, but we have not implemented an example of this.

STORE-6 requires that data should be backed up. As before, we determined that this was a low priority task because database tools exist to create automatic backups, and these are not key to the architecture of our system, and so we chose to not implement a backup solution.

\subsection{Further Considerations}
Throughout the project we have discussed the possibility of implementing some sort of security for the system. In the early stages of the project the customer was asked whether this would be desirable, and they said that it was not a key priority. However, the system is now at a stage where it could be used for some serious monitoring, and in the real world it would almost certainly be desirable for some sort of user authentication to be required before the configuration can be altered. 

\subsection{Problems Encountered}
\label{ssec:problems}
\textbf{Heisenbugs}. Several problems were encountered during the development of this second prototype. The most insidious of which were the bugs introduced by concurrency. When the scheduler was introduced, it ran several jobs in parallel. These jobs were originally coded under the assumption of single-threaded code and several bugs which when investigated in the debugger disappeared, hence they were ``Heisenbugs", disappearing upon measurement \cite{heisenbugs}. The reality is that the debugger changes the timing of the program and can often remove race conditions that previously existed. The tracking down of these bugs required a lot of time as none of us had extensive experience developing concurrent software.

\textbf{Dynamic class loading}. The intention of our plugin system was to allow the user to drop class files and/or JAR files into the plugin directory and have the HUMS automatically load the new code into the running HUMS. This turned out to be more complicated than first imagined. Taking care of security considerations and dependency considerations between plugins in particular. Instead of developing a bespoke system for dynamically loading classes and managing all plugins loaded, in the spirit of code reuse, an off-the-shelf plugin management system was selected. This system was satisfactory for managing plugins, but it lacked any features for dynamically loading classes off the disk when the files changed. It was considered to be too large a task to retrofit dynamic class loading on top of the library selected for this iteration of the prototype.

\textbf{Malicious server activity}. The server we rented to host our HUMS database was the target of an attack. The hosting company noticed suspicious activity emanating from this machine and closed it down. The server software on the machine had to be reinstalled which was a significant investment of time. We did not plan for this risk in our risk register, which was certainly an oversight. Thankfully no important data were on the machine which was taken over, so no data loss occurred.
