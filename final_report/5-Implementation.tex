%!TEX root = main.tex
\section{Implementation}

This period of the development was the most fruitful, many improvements and enhancements were made to our prototype presented in the interim report. This subsection shall describe each improvement in turn, considering any limitations identified which would be fixed in a future prototype iteration.
\subsection{Plugin system}
A major internal change was the employment of a plugin system. The plugin system was introduced in an attempt to make writing Plugins more accessible to Third Party developers, and to raise the level of abstraction in which new Plugins were created. When the HUMS starts up, it scans a particular plugin directory recursively, automatically loading any Java classes that implement the plugin interface.
%CORRECT
These plugins are then registered within the system, and other Plugins may query the plugin Plugin for plugins by name. When given a textual name to search for, the plugin system scans its database of plugins to find the corresponding Java object given the textual name. This object is then returned to the caller, who may interact with it as if they originally instantiated it.

Another selling point of the plugin system is that it facilitates system extension by a Third Party user (thus fulfilling the Modifiability quality attribute). In particular, an API can now be designed to allow users of the HUMS to extend its functionality by creating Java Archives and dropping them in the HUMS plugin directory. This means that our HUMS is open to extension yet closed for modification, that is, the open-closed principle has been honoured; one of the SOLID OOD principles. Dynamic class loading was an intended feature for this prototype, but due to unforeseen complexity described in \autoref{ssec:problems}, it was not finished in time for this prototype. By dynamic class loading, it is meant the ability for the system to constantly monitor the plugin directory for any changes to the plugins already loaded and to reload these upon change. By implication it would also be possible to load/unload plugins on an individual basis. The library selected to manage the plugins, JSPF, does not support such functionality and instead it must be simulated by unloading \textbf{all} plugins and then loading \textbf{all} plugins if it is desired for a single plugin to be reloaded. While this is certainly a shortcoming, JSPF did have a number of advantages; the most important being that it was straightforward to get going with a basic plugin architecture in a prototype. A future prototype would certainly have a more sophisticated ``hot-loading" mechanism.

Existing Plugins from our previous prototype, including the JVM monitoring Plugins and the web server ping Plugins were ported to the new plugin system. In addition, several new Plugins were created within the sense, analysis and report subsystems. These were the weather and stock analysis systems.

\subsection{Configuration}
As mentioned in the previous report the configuration system was not implemented due to time constraints, and instead many elements were hard-coded. In this prototype the configuration system has been included. This system was primarily designed as an interface for the UI to interact with our core HUMS. The use of an interface such as this decouples the UI code from the core application code. The configuration system is in charge of storing the data that a user is allowed to manipulate from the UI. The configuration utility contains methods for storing, deleting, altering and getting information on sensors, jobs, recurring jobs, and users. The configuration utility serialises and stores these elements as lists, as defined in the Config class. When the configuration utility is used to store these elements an ID is generated for the individual object so that they can be identified by both the UI and the HUMS. The procedure a user takes to manipulate the current set of data within the prototype is described in \autoref{ssec:userInterface}.

The configuration system has also tidied up inter-Plugin communication in our system. Data were communicated in an ad-hoc way in previous prototypes, for simple testing purposes. Data may be queried at a later time either by the same Plugin, or a different Plugin. The data are stored in external storage using the JSON file format. Other Plugins may ask the configuration system to fetch particular records from this JSON file.

As discussed, the configuration system stored elements in JSON. An example of a recurring job is given in \autoref{lst:examplejson}. The JSON object holds all of the data about the recurring job, such as an id, the job definition (which contains all of the tasks for the job), and a time interval, detailing how often the job should run. The JSON format presented here is the same as the JSON format used by the user interface.

\begin{lstlisting}[caption=Example JSON for Recurring Job, label=lst:examplejson, frame=single]
{
  "id" : 0,
  "job" : { "activeTask" : 2,
      "data" : { "data" : "",
          "recordIds" : [ 16038 ]
        },
      "id" : 0,
      "tasks" : [ { "controllerDestination" : "SENSE",
            "id" : 0,
            "PluginDestination" : "Ping",
            "settings" : { "address" : "www.google.com",
                "id" : "1"
              },
            "startDelay" : 0
          },
          { "controllerDestination" : "ANALYSIS",
            "id" : 0,
            "PluginDestination" : "PingAnalysis",
            "settings" : {  },
            "startDelay" : 0
          },
          { "controllerDestination" : "REPORT",
            "id" : 0,
            "PluginDestination" : "PingReport",
            "settings" : {  },
            "startDelay" : 0
          }
        ]
    },
  "timeInterval" : 5000
}
\end{lstlisting}

\subsection{Using a Web Server to Connect to the User Interface}
\label{sec:webserver}
In order for the application to communicate with the UI the HUMS was set up to use a web server. Jetty \cite{jetty} was embedded into the HUMS and used to provide a web server which the UI could communicate with in order to both push and retrieve information on components such as jobs and sensors. 

A number of handlers were defined which contained endpoints that the UI could send GET and POST requests to. A handler was created for each type of object stored in the config. These are: jobs, recurring jobs, sensors and tasks, which are handled in the classes JobHander, RecurringJobHandler, SensorHandler and TaskHandler respectively. Each of these handlers contains a specific route which the UI must send request to in order for them to be processed. For example, the UI must send a POST request to sensor/new with JSON containing the details for a new sensor. These handlers use the configuration utility to carry out the request.

\subsection{Logging}
A logger was also added in order to log all of the events in the system. The logger was created using log4j \cite{log4j}, a Java library for logging. Log4j specifies different severity levels: trace, debug, info, warn, and error. All errors, external access requests, and all configuration changes are logged, both to a file and to the standard output console. A number of logging messages were also created for debug purposes.

\subsection{Scheduler}
A scheduler has been added to the system that can execute jobs periodically or on a one-off basis. The scheduler is a singleton class based around the ScheduledExecutorService class, which is part of the concurrency package in Java. The SchedulerExecutorService class can take objects that implement the Runnable (or Callable) interface and periodically start them on a new thread. Its sequence diagram is shown in \autoref{fig:schedulerSeq}.

\begin{figure*}
    \centering
    \includegraphics[page=3,width=\textwidth,height=\textheight,keepaspectratio]{figures/architecture}
    \caption{Scheduler Sequence Diagram}
    \label{fig:schedulerSeq}
\end{figure*}

In a previous phase of development we came up with the requirements that the order in which tasks are executed should be decided by a priority associated with each task (MAIN-10). When thinking about how to implement such a scheduler in our system, we realised that we had been thinking of the scheduler executing jobs as a sequential process, but in fact this did not need to be the case. A job contains a set of tasks that are executed sequentially, but a job does not depend on other jobs in the system while it is executing, and so by placing every job on its own thread we do not need to consider job priorities. 

This is of course making the assumption that the host machine can handle all of the threads that our scheduler creates, which may not be the case. If in the future the customer finds that jobs are not executing when they should be because existing jobs are taking a long time to execute, then another approach for the scheduler will need to be considered, but we are satisfied that the ScheduledExecutorService is suitable, especially for a prototype system.

A new class was created called RecurringJob that is a thin wrapper for the Job class, but also contains information that is useful for the scheduler, namely an ID and the frequency with which the job should be executed. A set of recurring jobs is loaded from the configuration system when the scheduler is initialised, and these jobs are started. The scheduler contains an update method that can be called to refresh the set of scheduled jobs if the configuration has changed. The update method finds any jobs that have been added to the configuration, and also finds any jobs that it currently has scheduled for execution that are no longer present in the configuration, and updates itself accordingly. Any jobs that were previously in the configuration and continue to exist in the configuration will not be reset, unless their properties have been altered.

\subsection{Store Utility}
The store utility, which was created in the previous prototype, is also present in our current prototype. The store utility manages the access to the database backend, providing a connection and functions to query the database. It acts as a layer of abstraction such that the system does not have to handle raw SQL queries. 

The story utility consists of marshalls which convert between objects used by the HUMS and entries which are stored in the database. The marshalls provide the functionality for storing and retrieving data, as well as providing the option to call raw SQL queries for complex cases.
The data is stored as JSON objects, similar to the configuration utility. This means that the store utility and the configuration utility are able to easily communicate with each other. Data can also be stored in the form of key-value pairs. 

The store utility has changed since the last prototype. It is now a core part of the HUMS, rather than just being defined by the marshallers. It uses one single connection, rather than separate connections for different components of the system. This was a bottleneck in our previous prototype and thus has been improved. Similarly for the configuration system, the store utility moved over to using the Gson library for serialising and deserialising JSON. This provided a more robust system for creating JSON rather than using our own approach. The marshaller was also generalised, rather than having separate marshallers for different types of data, thus making the system more resilient to any future changes to the data architecture.

\subsection{User Interface}
\label{ssec:userInterface}
As mentioned in the previous report no UI was designed for the last prototype. In this prototype the user interface has been designed and handles all of the interaction with the HUMS. The user is able to manage sensors, jobs, recurring jobs, and tasks through the UI. Moreover, report plugins which return data can be displayed in the UI in the form of graphs as long as they comply with the data format used. The web application communicates with the HUMS embedded server over the HTTP protocol and all the data passed is encoded as JSON.

Users are able to both see and alter the state of the system using the UI (\autoref{fig:guiShowJobs}). The report section shows information about the HUMS as well as graphs based on the report plugins. Following is a list of main pages:
\begin{itemize}
  \item {home page, showing statistics of the HUMS}
  \item {configuration page, allowing the user to view and add new sensors and jobs, and set their schedule (\autoref{fig:guiAddJob})}
  \item {a reports section which has a page for each report plugin that returns data to the UI}
  \item {a log viewer, which shows all of the log messages output by the HUMS}
\end{itemize}

The web application was created using the AngularJS framework \cite{AngularJS} in order to provide experience of a regular desktop application without the need to reload a website after every major user action. Adding, editing and removing elements can be seen instantly by the user. Each page of the web application has a unique URL and routing is handled by AngularJS. Therefore the users can bookmark pages they used most frequently and back and forward buttons work correctly.

At the beginning a standard Bootstrap theme \cite{Bootstrap} was used for styling. It allowed the team to focus on the functionality of the web application without compromising its visual aspect. Nevertheless the design did not reflect the nature of a HUMS as it did not resemble a dashboard or a control panel. After the team analysed how much effort would need to be put into styling all the necessary elements it turned out that there would not be enough time to do that. Therefore a freely available theme was selected - AdminLTE \cite{adminLTE}. Its main layout resembles a dashboard and it offers several components which make it easy to group elements such as sensors or jobs (\autoref{fig:guiShowSensors}).

Furthermore, two Javascript libraries are used in order to draw graphs. They are complementary and were chosen due to the number of graph types they support and their ease of use. Raphael \cite{Raphael} is used to make vector graphics in Javascript but it does not offer any built-in graphs. Morris.js \cite{Morris} provides high-level methods such as Morris.Line which take a dataset and create a graph using Raphael`s methods.

All the components mentioned in the preceding paragraphs were chosen based on the requirement that the web application should follow the principles of responsive design and be compatible with touch devices. Hence the resulting web application makes up for the lack of mobile-specific applications as it can be run in any modern web browser.

Currently data returned by report plugins can only be displayed as a line graph. This limitation is specific to the most recent prototype and it should instead be considered a showcase of what can be done in the UI. Due to the fact that the UI is decoupled from the main backend system the interface can be implemented on any platform that can execute HTTP calls. As a result adding, for instance, a mobile application will not require any changes to the main HUMS and can be performed even after the system is deployed.

The UI, however, is not used to add new plugins to the system. This was done on purpose because new plugins should only be added by users who are familiar with HUMS architecture and have access to the containing server.

In order to support the system Data Flow described in \autoref{ssub:data_flow} the UI makes the following provisions:
\begin{enumerate}
  \item {Add a sensor and indicate if it will push data to the system or if the system will have to poll it for data}
  \item {Add a job definition. It should consist of one of the sensors and a sequence of Tasks returned by the HUMS' Plugin manager. Each task may be accompanied by an arbitrary number of settings expressed as key-value pairs. Corresponding screen is shown in \autoref{fig:guiAddJob}}
  \item {If the job was defined with a `push' sensor the UI setup is finished but the sensor ID must be appended to every message pushed by that sensor for identification purposes. Otherwise it is necessary to schedule the newly created job definition. This is achieved by selecting a job definition and associating it with an interval in milliseconds}
\end{enumerate}


\begin{figure*}
\includegraphics[width=\linewidth]{figures/screenshots/guiShowJobs}
\caption{Showing the existing jobs in the GUI}
\label{fig:guiShowJobs}
\end{figure*}

\begin{figure*}
\includegraphics[width=\linewidth]{figures/screenshots/guiAddJob}
\caption{Adding a job in the GUI}
\label{fig:guiAddJob}
\end{figure*}

\begin{figure*}
\includegraphics[width=\linewidth]{figures/screenshots/guiShowSensors}
\caption{Showing the existing sensors in the GUI}
\label{fig:guiShowSensors}
\end{figure*}

\subsection{Iron.io} 
It was recognised early on that a subscription architecture would be appropriate given the nature of data arriving at unpredictable times. This problem was solved by using a message queuing service which allows clients to subscribe to certain types of data but not others. The service chosen was IronMQ, which is such a message queuing with cloud-optimised performance. 

An overview of what IronMQ provides is shown in \autoref{fig:ironmq}, taken from the IronMQ documentation. Its associated use case is presented in \autoref{fig:ironMqUse}.

\begin{figure*}
\centering
\includegraphics[width=.8\textwidth,height=\textheight,keepaspectratio]{figures/ironMQ}
\caption{IronMQ flow\label{fig:ironmq}}
\end{figure*}

\begin{figure*}
    \centering
    \includegraphics[page=1,width=\textwidth,height=\textheight,keepaspectratio]{figures/architecture}
    \caption{IronMQ Use Case Diagram\label{fig:ironMqUse}}
\end{figure*}

\begin{figure*}
    \centering
    \includegraphics[page=2,width=\textwidth,height=\textheight,keepaspectratio]{figures/architecture}
    \caption{IronMQ Sequence Diagram\label{fig:ironMqSeq}}
\end{figure*}

Our HUMS subscribes to content by making a request to the IronMQ cloud service. Currently only one queue is used to which all Push Sensors push data. In turn, all messages from that queue are delivered to the endpoint defined in HUMS. Each message must contain a sensor ID which is used by the IronMQ Handler to decide which Job is to be run. This is depicted in \autoref{fig:ironMqSeq}.

This describes how data are delivered to the Plugins which have requested particular types of data. The act of pushing data to the message queue is symmetrical. In order for the data to make its way into our system from the IronMQ cloud service, an end-point needed to be defined within our system. This end-point was implemented using the Jetty HTTP server within our HUMS (\autoref{sec:webserver}). When data are pushed to IronMQ from external devices monitored by our HUMS, these data are quickly delivered to the Jetty server which then schedules the jobs for execution. IronMQ uses a Post/Get/Delete paradigm, in which data are first posted to the respective queue and then they are retrieved (get) using a Jetty handler. The data are held for 1 minute until the Jetty handler responds, implying the data are to now be deleted, ensuring that the data are only processed once. If our HUMS (via Jetty) does not complete the task after 1 minute, then IronMQ will revoke this session's hold on the data and put them back on the queue, ensuring no data have been lost.

All of the operations are performed using IronMQ's REST API, which makes using IronMQ a pleasant experience.

