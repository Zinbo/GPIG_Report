%!TEX root = main.tex
\section{Architecture}
\label{sec:Architecture}

In the Interim Report we outlined the features that were to be implemented in the next prototype in order to fulfil the requirements of the HUMS, and created a development plan. The following four milestones were set:

\begin{enumerate}
\item Evaluate the existing system
\item Implement the scheduler, configuration system and dynamic class loading
\item Implement a user interface
\item Implement test applications
\end{enumerate}

This section will begin by discussing the decision to continue to build on the first prototype, then it will discuss the updated architecture, finishing by pointing out which quality attributes were chosen and how they manifest in the architecture. The test applications will be discussed in a later section.
\subsection{The existing system}
An overview of the existing system was given in the previous section. Before starting the development of the second prototype we had to evaluate the good and the bad points of the first prototype, and decide on what to keep and what to change.

The first decision was about the language used for the core of the system. In the first prototype Java 7 was used. There were no significant problems with it as Java is a flexible OO language, and so the team concluded that there was no need to re-write the existing codebase in a new language. In the time between finishing the first prototype and starting the second, Java 8 was released. Two features that would have made a significant difference were Java 8's introduction of a lexical means to manipulate closures (lambda expressions) and default methods in interfaces. Both of these features would have added significantly to the clarity of our design. We considered switching to Java 8, however we decided that it was better to keep backwards compatibility for the time being as Java 8 is not as widely available as Java 7.

The second decision was whether we should dismiss the first prototype. Even though initially the team had planned to make employ throw-away prototyping, due to the fact that the architecture would evolve rather than change completely the decision was made to base the second prototype on the first one.

We also discussed the possibility of replacing our existing database system. We were using MySQL and considered switch to a NoSQL system. We decided that the cost of doing so outweighed any perceived benefits for our prototype because the team lacked the necessary skills and expertise.

An evaluation of the existing architecture was performed, and some changes to the design were made. These changes are detailed in the following subsections - some were decided upon before development started, while others were a result of finding problems during development with the current architecture.

\subsection{Architectural Changes}
\subsubsection{Deployment}
Figure \ref{fig:deployment} shows a deployment diagram for the system. The main HUMS application, database, user interface and SNMP server will all be hosted on a server. The push and poll sensors as well as the IronMQ components are separate to the system and so are displayed as being outside of the server.
\begin{figure*}
    \centering
    \includegraphics[page=7,width=.8\textwidth,height=\textheight,keepaspectratio]{figures/architecture}
    \caption{System Deployment Diagram}
    \label{fig:deployment}
\end{figure*}
\subsubsection{Message Bus} % or lack of…?
In our first prototype, the architecture revolved around a very simple message bus class that routed messages around the system. We recognised that this is a serious bottleneck and therefore discussed alternatives. In the previous reports we mentioned using an Enterprise Service Bus (ESB) in order to route messages throughout the system to individual Plugins. We briefly looked into JBoss's implementation of an ESB for Java \cite{JbossEsb}. We realised that whilst JBoss ESB was a powerful tool that would allow us to distribute the system across many servers, it was unnecessarily complex for a prototype.

Instead we re-evaluated the purpose of the message bus, and decided that it was only Plugins within the same Job that needed to communicate with each other, so each Job could have a shared object that the Plugins can use to communicate. Each Task in a Job executes sequentially, so there is no need to worry about two Tasks writing to the shared object simultaneously. This object is called JobData, and is typically used to point to records in the database that may be of use to the next task.

It used to be the case that a Task would define the next Task that should be executed. So a sense type Task might set that the next Task to be executed is an analysis type Task. This was done so that if an analysis Plugin decided that it required further sense readings then it could set the next Task to be a sense type Task, rather than the typical report type Task. In other words, the flow of events could be customised dynamically by the system. However after some consideration we concluded that we could think of no situations where this would be absolutely critical. Moreover, the implementation was convoluted and so in the new architecture the order in which Tasks are executed is predetermined by the user. A Job encapsulates a set of Task objects and executes each one sequentially. In theory each Task could itself execute another Task if it was required, although this has not been tested.

\subsubsection{Scheduler}
The idea in the previous system was that a scheduler would connect to the message bus and pass Jobs onto the bus at user-defined intervals. However as we have now decided on essentially removing the message bus, this approach has to be reconsidered. Because Jobs do not have to communicate with each other, and the message bus does not connect all Jobs together, they are essentially self-contained units that have an execute function. This makes implementing a scheduler fairly simple, because the scheduler can just set a job running at any time, without having to worry about any other Jobs that are executing, or having to pass a Job onto a message bus with a destination set.

\subsubsection{Configuration System}
\label{ssub:configurationSystem}
As already discussed, no consideration was made about how the Configuration System would be implemented when we were developing the first prototype. As mentioned the Configuration System stores data on all created Jobs, Sensors, Tasks and Plugins. This information is then used both internally in the HUMS and by the UI. The Configuration System shall store data such that if the HUMS somehow goes down the configuration data will not be lost.  Architecturally the Configuration System is not difficult -- it simply has a set of static methods that allow the configuration to be read and written to. The Configuration System is also responsible for automatically generating ID's so that elements can be identified by the User in the UI.
\subsubsection{Plugin system}
Similarly to the configuration system, the plugin system is relatively self-contained. What it needs to do is find a set of plugin implementations and make them available to the Configuration System and other Plugins which request them; it does not rely on any other part of the system.

The architecture of the Plugin System was forced by the way in which the Java Simple Plugin Framework (JSPF) library \cite{jspf} discovers plugins. JSPF employs a reflection-based approach to locating Plugins. Plugins are discovered by searching for all classes implementing a particular interface. Once all the Plugins have been discovered, the list of Plugins are sent to the Configuration System to store for later reference. Now other modules in the system can query the Plugin module for classes by name. The Plugin System shuts down when the HUMS shuts down. An overview of these states is shown in \autoref{fig:plugin_state_diagram}.

\begin{figure*}
\centering
\includegraphics[width=.9\textwidth,height=\textheight,keepaspectratio]{figures/plugin_state_diagram}
\caption{A UML state diagram showing the state transitions of the plugin module.}
\label{fig:plugin_state_diagram}
\end{figure*}

With reference to the diagram in \autoref{fig:plugin_architecture}, to find all Plugins in the system, JSPF would be instructed to find all Plugins implementing the \texttt{HumsPlugin} interface. To find all sensor Plugins, JSPF would be instructed to find all Plugins implementing the \texttt{SensePlugin} interface and so on. The store Plugin interface is not presented here as no store Plugins were developed. Each Plugin type has a corresponding abstract class: \texttt{ReportPluginBase}, \texttt{AnalysisPluginBase} and \texttt{SensePluginBase}. Each provides functionality for storing data in the appropriate table of the database as a convenience for implementers. All Plugins extend the appropriate plugin base.

\begin{figure*}
\centering
\includegraphics[scale=0.5]{figures/plugin_architecture}
\caption{A UML class diagram showing the architecture of the Plugin system. Each concrete plugin implements one of the \texttt{*Base} classes. JSPF exploits the inheritance hierarchy to locate particular plugins.}
\label{fig:plugin_architecture}
\end{figure*}

\subsubsection{Data Flow} % (fold)
\label{ssub:data_flow}
The architecture supports two forms of communication: pushing and polling. The former indicates that a sensor will send data to the HUMS whenever it wishes; it is the sensor who initiates the request. The latter indicates that it is the HUMS who initiates the request at an interval specified by the user.

Once the data is present within the HUMS it is passed to the respective store type Plugin where its format can be changed or other kind of sanitisation performed. This is then stored in a database for future use in other Plugins. In a typical lifecycle data travels from a sensor through a sense type Plugin, then through an analysis type Plugin to a report type Plugin. The number and order of modules can be customised so that non-trivial analysis and feedback loops can be introduced. A graphical representation of the Data Flow is shown in Figures \ref{fig:pluginDataFlow} and \ref{fig:taskSeq}.
% subsubsection data_flow (end)

\begin{figure*}
    \centering
    \includegraphics[page=5,width=.9\textwidth,height=\textheight,keepaspectratio]{figures/architecture}
    \caption{Plugin Data Flow Diagram}
    \label{fig:pluginDataFlow}
\end{figure*}


\begin{figure*}
    \centering
    \includegraphics[page=6,width=\textwidth,height=\textheight,keepaspectratio]{figures/architecture}
    \caption{Task Sequence Diagram}
    \label{fig:taskSeq}
\end{figure*}

\subsubsection{User Interface}
\label{sec:wireframes}
In the previous report we included some sketches that showed how we would like the UI to look. The sketches were very basic, and so before work began on the UI we came up with a set of wireframes that more precisely showed what the team wanted to be contained. Subsections \ref{ssub:configurationSystem} and \ref{ssub:data_flow} were used extensively to come up with the necessary UI elements. Figures \ref{fig:wireframe_1} - \ref{fig:wireframe_7} in \autoref{app:wireframes} show the wireframes that were designed for the application. These wireframes do not represent the final layout; they were presented to the Customer to receive feedback and elicit requirements for the interface which were taken into account while creating the UI for the most recent prototype.

\subsection{Architecture Quality Attributes} % (fold)
\label{sub:architecture_quality_attributes}
Quality attributes are an industry standard way of expressing the most important features (other than functionality) of the system \cite{Bass:2003:SAP:773239}. They must be picked before any work on the architecture begins. The software architect uses them throughout the whole design process. In the previous report the quality attributes were not mentioned even though they had been chosen at the beginning of the project.

Based on the interaction with the Customer and following the advice from \cite{Bass:2003:SAP:773239} the following two attributes were chosen as most important:
\begin{itemize}
	\item {Modifiability}
	\item {Interoperability}
\end{itemize}
The Customer has emphasised on multiple occasions that the HUMS should be \emph{tailorable}. This meant that the system should not be restricted to a single usage domain or way of communicating. Therefore modifiability and interoperability were picked as quality attributes to drive the architecture. The former is important because the HUMS creators may not be aware of a particular communication protocol which is extensively used in, for instance, the mining industry. In case of a tailorable HUMS adding support for a new communication protocol should require very little changes to the existing system on top of adding the new feature. The latter quality attribute is deemed important because there is a myriad of existing sensors and company-specific monitoring systems which can possibly be reused with the HUMS being created. Hence it is of paramount importance for it to be able to interface with external entities which may make use of an exotic data format, also because doing otherwise could discourage potential customers.

Both quality attributes have been taken into account while creating the software architecture. A modular approach to communication protocols has been adopted as can be seen in \autoref{fig:deployment}. Provisions for two of them are highlighted -- SNMP Server within the main System boundaries as well as IronMQ which is an external message queue service. The HUMS can be modified by adding a new communication protocol and registering it in the system.

Furthermore, the Push and Poll Sensor components in \autoref{fig:deployment} show how interoperability is achieved. They represent any artefact which is capable of sending data to the HUMS using one of the supported communication methods. It may be a physical device which uses SNMP or an application which reads data from a physical sensor and sends it to HUMS over HTTP. As a result the system can interface with virtually any existing sensor.
% subsection architecture_quality_attributes (end)

\subsection{Design}
The summarise the design of our prototype, an overview of Users, Tasks, Sensors, Jobs and RecurringJobs is shown in \autoref{fig:class:config}. This figure also shows the
ownership relations between the classes.  The classes instrumental in the HUMS loading phase are diagrammed in \autoref{fig:class:core}. Finally, the classes related to the storage and retrieval of data are diagrammed in \autoref{fig:class:store}.

\begin{figure*}
\centering
\includegraphics[width=.9\textwidth,height=\textheight,keepaspectratio]{figures/classDiagrams/Core.png}
\caption{UML Class Diagram of classes related to the main HUMS entry point, scheduler, task controlling and plugin classes}
\label{fig:class:core}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=.8\textwidth,height=\textheight,keepaspectratio]{figures/classDiagrams/Config.png}
\caption{UML Class Diagram showing classes related to the configuration of Users, Tasks, Sensors, Jobs and Recurring Jobs}
\label{fig:class:config}
\end{figure*}

\begin{figure*}
\centering
\includegraphics[width=.9\textwidth,height=\textheight,keepaspectratio]{figures/classDiagrams/Store.png}
\caption{UML Class Diagram showing classes related to the storage and retrieval of data}
\label{fig:class:store}
\end{figure*}