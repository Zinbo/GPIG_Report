%!TEX root = main.tex
\section{Requirements}

\label{sec:Requirements}

In order to further refine our requirements we took a top-down approach to the system. We created a number of wireframes (which are discussed in \autoref{sec:Architecture}) in order to assess how a User might interact with the system. Through the analysis of these wireframes a number of requirements were created and refined.

The user stories which were created were also used to refine the requirements. As discussed in the previous report, these stories were derived from a number of different users' perspectives. These were:

\begin{description}
\item[Developer] Stories from a developer concern requirements for best practices, or to research a potential technology.
\item[Administrator] Stories from an administrator concern requirements which allow the administrator to control and modify the HUMS.
\item[Third-party Developer] Stories from a third-party developer concern creating and modifying sense, analysis, and reporting modules.
\item[HUMS Analysts] Stories from a HUMS analyst concern the data flow through the system such that reports are generated which reflect sensed data.
\item[Scheduler] stories from the scheduler concern the time constraints, priorities, and ordering of tasks through the system.
\item[Sensor] Stories from a sensor concern the delivery of readings from a sensor to the system.
\end{description}

These user stories helped us to re-evaluate the system, create a more concrete prototype, and address any conflicts and contradictions that we had during the development process.

As we took an agile approach to development, a number of requirements were also created and refined throughout the development of this prototype. The prototype presented in this report has matured from the initial prototype presented in the previous report. As such, the design and requirements have also matured.
\subsection{New Requirements}
As mentioned, a number of new requirements were created which can be seen in \autoref{tab:newreq}. Many of these requirements were realised as the Configuration System, Scheduler and User Interface (UI) matured from the last prototype. A message going through the system was also defined more concretely. What was called a message in the previous prototype is now called a Job. A Job is composed of a number of \emph{tasks}, where each task defines the destination for that task (i.e. what Modules the Job is to be routed to for that task), and the data necessary for that task to carry out its operation. We made a distinction between a Job which is set to be run only once against a Job which is to be run on a regular schedule (a Recurring Job).

As can be seen in Table \ref{tab:newreq}, a number of requirements relate to the configuration and UI. These elements were minimal in the last prototype, but they have been developed and refined during this phase of development.

These changes were presented to the customer in the form of wireframes via email, which is discussed in \autoref{sec:custInteraction}.



\end{multicols}
\begin{longtable}{|p{3cm}|p{12cm}|}
\hline
ID & Requirement \\
\hline
MAIN-22 &
The scheduler system shall be able to accept new recurring jobs. \\
\hline
MAIN-23 &
A user shall be able to add a job definition, specifying the order of modules to be used. \\
\hline
MAIN-24 &
A user shall be able to trigger a job, using a pre-defined job definition. \\
\hline
MAIN-25 &
A user shall be able to schedule a recurring job, using a pre-defined job definition,  specifying the time interval at which the job should run.. \\
\hline
MAIN-26 &
A user shall be able to delete a job definition. \\
\hline
MAIN-27 &
A user shall be able to delete a recurring job schedule. \\
\hline
MAIN-28 &
A user shall be able to add a new sensor definition, specifying the details necessary to communicate with the sensor. \\
\hline
MAIN-29 &
A user shall be able to delete a sensor definition. \\
\hline
MAIN-30 &
The system shall allow new users to be defined. \\
\hline
MAIN-31 &
The system shall allow users to be deleted. \\
\hline
MAIN-32 &
The system shall store all created job definitions in a configuration file. \\
\hline
MAIN-33 &
The system shall store all recurring job schedules in a configuration file. \\
\hline
MAIN-34 &
The system shall store all sensor definitions in a configuration file. \\
\hline
MAIN-35 &
The system shall store all user details in a configuration file. \\
\hline
\caption{New requirements}
\label{tab:newreq}
\end{longtable}
\begin{multicols}{2}

\subsection{Refined Requirements}
\autoref{tab:refinedreq} shows the requirements which have been refined since the last report. Only two requirements were refined. MAIN-12 was changed such that it refers to Recurring Jobs rather than Tasks. This is because of a change in terminology from the last report. The wording of MAIN-3 was also changed. Previously MAIN-3 said that a user was allowed to log in, rather than the user is required to log in. Logging in to the system is mandatory, and thus the wording of the requirement was changed to reflect this.

\end{multicols}
\begin{longtable}{|p{3cm}|p{12cm}|}
\hline
ID & Requirement \\
\hline
MAIN-3 &
The system shall require a user to log in using the correct credentials. \\
\hline
MAIN-12 &
The scheduler system shall trigger recurring jobs every given time interval. \\
\hline
\caption{Refined requirements}
\label{tab:refinedreq}
\end{longtable}
\begin{multicols}{2}

\subsection{Unchanged Requirements}
The requirements given in \autoref{tab:unchangedreq} are unchanged from the previous report. These requirements still fit our and the customers' vision of the system.
\end{multicols}
\begin{longtable}{|p{3cm}|p{12cm}|}
\hline
ID & Requirement \\
\hline
MAIN-1 &
The system shall provide a command API which reporting modules can use to send commands to sensors. \\
\hline
MAIN-2 &
The system shall log all time-outs from modules. \\
\hline
MAIN-4 &
A role shall have a set of permissions which a user of that role is granted. \\
\hline
MAIN-5 &
A user shall have a role. \\
\hline
MAIN-6 &
The system shall allow new roles to be defined. \\
\hline
MAIN-7 &
The system shall allow modules to define their own log messages. \\
\hline
MAIN-8 &
The system shall require that each log message includes a severity level. \\
\hline
MAIN-9 &
The system shall require that each log message includes the date and time it was generated. \\
\hline
MAIN-10 &
The system shall log all external input and output requests. \\
\hline
MAIN-11 &
The system shall log all configuration changes to a subsystem. \\
\hline
MAIN-13 &
The scheduler subsystem shall allow the order tasks to be modified, including at runtime. \\
\hline
MAIN-14 &
The scheduler subsystem shall have the ability to prioritise important tasks over low priority tasks. \\
\hline
MAIN-15 &
The scheduler subsystem shall have the ability to adjust the timing requirements on a job in order to reduce the load on the system. \\
\hline
MAIN-18 &
The system shall log all errors. \\
\hline
MAIN-19 &
The system itself shall be monitorable. \\
\hline
MAIN-20 &
The system shall store its current configuration in storage. \\
\hline
MAIN-21 &
The system shall be administrable through a GUI. \\
\hline
SENSE-1 &
The sense subsystem shall allow sense modules to be added, including at run time. \\
\hline
SENSE-2 &
The sense subsystem shall allow sense modules to be modified, including at run time. \\
\hline
SENSE-3 &
The sense subsystem shall allow sense modules to be removed, including at run time. \\
\hline
SENSE-4 &
The sense subsystem shall allow a sense module to define how long data should be stored for. \\
\hline
SENSE-5 &
The system shall allow the user to set a polling time interval for a sensor. \\
\hline
SENSE-6 &
The system shall allow the user to set a time limit before sense module abortion. \\
\hline
SENSE-7 &
A sense module shall be able to poll a sensor for data. \\
\hline
SENSE-8 &
A sense module shall be able to pull data from a sensor. \\
\hline
SENSE-9 &
A sense module shall be able to receive pushed data from a sensor. \\
\hline
SENSE-10 &
A sense module shall be able to determine from which sensor and at what time data was received \\
\hline
SENSE-11 &
The sense subsystem shall allow sense modules to trigger analysis modules. \\
\hline
SENSE-12 &
A sense module shall not handle data which does not conform to a defined format. \\
\hline
SENSE-15 &
The system shall provide an extendable interface for third-party sense modules. \\
\hline
ANALYSIS-1 &
The analysis subsystem shall allow analysis modules to be added, including at run time. \\
\hline
ANALYSIS-2 &
The analysis subsystem shall allow analysis modules to be modified, including at run time. \\
\hline
ANALYSIS-3 &
The analysis subsystem shall allow analysis modules to be removed, including at run time. \\
\hline
ANALYSIS-4 &
The system shall allow the user to set a triggering interval for an analysis module. \\
\hline
ANALYSIS-5 &
The system shall allow the user to trigger analysis modules. \\
\hline
ANALYSIS-6 &
The system shall allow the user to set a time limit before analysis module abortion. \\
\hline
ANALYSIS-8 &
The system shall provide an extendable interface for third-party analysis modules. \\
\hline
REPORT-1 &
The reporting subsystem shall allow reporting modules to be added, including at run-time. \\
\hline
REPORT-2 &
The reporting subsystem shall allow reporting modules to be modified, including at run-time. \\
\hline
REPORT-3 &
The reporting subsystem shall allow reporting modules to be removed, including at run-time. \\
\hline
REPORT-4 &
The reporting subsystem shall allow reporting modules to generate reports after a configurable delay. \\
\hline
REPORT-8 &
The reporting subsystem shall be able to generate reports upon request. \\
\hline
REPORT-11 &
The report subsystem shall notify users upon report generation. \\
\hline
REPORT-12 &
The system shall provide an extendable interface for third-party report modules. \\
\hline
REPORT-13 &
The reporting subsystem shall allow report creation based on historical data. \\
\hline
STORE-3 &
The store subsystem shall be able to accept manual uploading of data. \\
\hline
STORE-4 &
The store subsystem shall provide an API through which modules can store and retrieve data. \\
\hline
STORE-6 &
The store subsystem shall back up its data after the specified time limit. \\
\hline
STORE-7 &
Sense modules shall store all data received using the store utility. \\
\hline
STORE-8 &
Analysis modules shall have the ability to store data using the store utility. \\
\hline
\caption{Unchanged requirements}
\label{tab:unchangedreq}
\end{longtable}
\begin{multicols}{2}
